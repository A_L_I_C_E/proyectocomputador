> Fabricio Parra. 30.837.006 \
> Elio Guarate. 28.182.524

# Computadora de Streaming de Videojuegos ($2500)

1. Memoria RAM: G.Skill Trident Z5 RGB 64 GB (Pack 2 x 32GB) DDR5-6400 MHz - Elegimos esta memoria RAM por su alta velocidad y capacidad,
lo que proporcionará un rendimiento óptimo en juegos y tareas multiarea. EL precio
promedio es de alrededor de $224.99 dólares.

2. Unidad de almacenamiento: Samsung 980 Pro 2 TB M.2-2280 PCIe 4.0 X4 NVME SSD - Esta unidad de estado sólido (SSD) ofrece
velocidades de lectura/escritura extremadamente rápidas, lo que resulta en
tiempos de carga reducidos. Además, tiene una capacidad suficiente para almacenar
varios juegos. El precio promedio es de alrededor $119.99 dólares.

3. Fuente de alimentación: Corsair SF750 750 W 80+ Platinum Certified Fully Modular SFX - Esta fuente de alimentación certificada Platinum 80+ de
750 Watts ofece una eficiencia energética alta y confiable. Además, proporciona
suficiente suficiente potencia para alimentar los componentes del sistema. El
precio promedo es de alrededor de $149.99 dólares.

4. Refrigerador: Corsair iCUE H150i ELITE CAPELLIX XT 65.57 CFM Liquid CPU Cooler -
Este refrigerador de CPU ofrece una excelente capacidad de enfriamiento a un precio
asequible. Además su iluminación RGB añade un toque estético al sistema. EL precio
promedio es de alrededor de $199.99 dólares.

5. Ventiladores: Corsair LL120 43.25 CFM 120 mm Fan. Lian Li UNI FAN SL V2 64.5 CFM 120 mm Fans 3-Pack - Estos
ventiladores RGB de 120 mm son silenciosos y proporcionan un buen flujo de aire para
mantener el sistema refrigerado. Recomendaría tener al menos tres de estos ventiladores
para una buena circulación de aire. EL precio promedio es de alrededor $38.99 y $82.90 dólares respectivamente.

6. Chasis: NZXT H9 Flow ATX Mid Tower Case - Este chasis es elegante y cuenta con una buena administración de cables,
una ventana lateral de vidrio templado y un buen flujo de aire. Además, tiene un precio
razonable. EL precio promedio es de alrededor $159.99 dólares.

7. Pasta térmica: Thermal Grizzly Hydronaut 3.9 g - Esta pasta térmica es ampliamente reconocida por su alta
conductividad térmica y facilidad de aplicación. El precio promedio es de alrededor de
$10.43 dólares.

8. Placa madre: Asus ROG STRIX B650E-F GAMING WIFI ATX AM5 Motherboard - Esta placa madre ofrece una exelente calidad de construcción, soporte
para procesadores de última generación y una amplia gama de características para los gamers.
El precio promedio es de alrededor de $299.99 dólares.

9. Procesador: AMD Ryzen 9 7900X 4.7 GHz 12-Core Processor - Este procesador de 12 núcleos y 24 hilos es excelente para juegos
y tareas multiarea intensivas. Ofrece un alto rendimiento y una arquitectura de última
generación. EL precio promedio es de alrededor de $432.00 dólares.

10. Tarjeta gráfica: XFX Speedster MERC 310 Black Edition Radeon RX 7900 XT 20 GB Video Card - Esta
tarjeta gráfica ofrece un excelente rendimiento en juegos y soporta tecnologías de trazado
de rayos. Es una opción sólida para juegos de alta calidad visual. El precio promedio es
de alrededor de $779.99 dólares.

|Componentes|Precio|Link|
|-----------|------|----|
|Memoria RAM: G.Skill Trident Z5 RGB 64 GB (Pack 2 x 32GB) DDR5-6400 MHz|$224.99|https://www.amazon.com/dp/B0BJ7X9P1W?tag=pcpapi-20&linkCode=ogi&th=1&psc=1|
|Samsung 980 Pro 2 TB M.2-2280 PCIe 4.0 X4 NVME SSD|$119.99|https://www.amazon.com/dp/B08RK2SR23?tag=pcpapi-20&linkCode=ogi&th=1&psc=1|
|Corsair SF750 750 W 80+ Platinum Certified Fully Modular SFX |$149.99|https://www.amazon.com/dp/B07M63H81H?tag=pcpapi-20&linkCode=ogi&th=1&psc=1|
|Corsair iCUE H150i ELITE CAPELLIX XT 65.57 CFM Liquid CPU Cooler|$199.99|https://www.amazon.com/dp/B0BQJ6QL7L?tag=pcpapi-20&linkCode=ogi&th=1&psc=1|
|Corsair LL120 43.25 CFM 120 mm Fan|$38.99|https://www.amazon.com/dp/B075VDGSJ8?tag=pcpapi-20&linkCode=ogi&th=1&psc=1|
|Lian Li UNI FAN SL V2 64.5 CFM 120 mm Fans 3-Pack|$82.90|https://www.amazon.com/dp/B0BLSNX7R9?tag=pcpapi-20&linkCode=ogi&th=1&psc=1|
|NZXT H9 Flow ATX Mid Tower Case|$159.99|https://www.amazon.com/dp/B0BFZZ3ZWZ?tag=pcpapi-20&linkCode=ogi&th=1&psc=1|
|Thermal Grizzly Hydronaut 3.9 g| $10.43|https://www.amazon.com/dp/B00ZJSXE2Y?tag=pcpapi-20&linkCode=ogi&th=1|
|Asus ROG STRIX B650E-F GAMING WIFI ATX AM5 Motherboard|$299.99|https://www.amazon.com/dp/B0BHMTYZKZ?tag=pcpapi-20&linkCode=ogi&th=1&psc=1 |
|AMD Ryzen 9 7900X 4.7 GHz 12-Core Processor|$432.00|https://www.amazon.com/dp/B0BBJ59WJ4?tag=pcpapi-20&linkCode=ogi&th=1&psc=1 |
|XFX Speedster MERC 310 Black Edition Radeon RX 7900 XT 20 GB Video Card|$779.99|https://www.amazon.com/dp/B0BNLSZDCX?tag=pcpapi-20&linkCode=ogi&th=1&psc=1|
|TOTAL|$2499.25||
